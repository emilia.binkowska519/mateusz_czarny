import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SpaceShooter extends JFrame implements ActionListener, KeyListener {

    private Timer timer;
    private int playerX, playerY, playerSpeed;
    private List<Enemy> enemies;
    private int bulletX, bulletY, bulletSpeed;
    private boolean isShooting;

    public SpaceShooter() {
        setTitle("Space Shooter");
        setSize(800, 600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setResizable(false);

        playerX = 400;
        playerY = 500;
        playerSpeed = 5;

        enemies = new ArrayList<>();
        initializeEnemies();

        bulletX = -1;
        bulletY = -1;
        bulletSpeed = 10;
        isShooting = false;

        timer = new Timer(10, this);
        timer.start();

        addKeyListener(this);
        setFocusable(true);

        // Dodajemy panel do ramki
        SpacePanel spacePanel = new SpacePanel();
        add(spacePanel);
    }

    private void initializeEnemies() {
        for (int i = 0; i < 5; i++) {
            Random rand = new Random();
            int x = rand.nextInt(750);
            int y = rand.nextInt(300);
            enemies.add(new Enemy(x, y));
        }
    }

    public void actionPerformed(ActionEvent e) {
        update();
        repaint();
    }

    public void update() {
        if (isShooting) {
            bulletY -= bulletSpeed;
            if (bulletY < 0) {
                isShooting = false;
                bulletY = -1;
            }
        }

        for (Enemy enemy : enemies) {
            enemy.move();
        }

        checkCollisions();
    }

    private void checkCollisions() {
        Rectangle bulletRect = new Rectangle(bulletX, bulletY, 5, 10);

        for (Enemy enemy : enemies) {
            Rectangle enemyRect = new Rectangle(enemy.getX(), enemy.getY(), 30, 30);

            if (bulletRect.intersects(enemyRect)) {
                isShooting = false;
                bulletY = -1;
                enemy.resetPosition();
            }
        }
    }

    // Klasa reprezentująca przeciwnika
    private class Enemy {
        private int x, y;
        private int speed;

        public Enemy(int x, int y) {
            this.x = x;
            this.y = y;
            this.speed = 2;
        }

        public void move() {
            y += speed;
            if (y > getHeight()) {
                resetPosition();
            }
        }

        public void resetPosition() {
            Random rand = new Random();
            x = rand.nextInt(750);
            y = rand.nextInt(300) - 400; // Pojawiają się ponownie powyżej ekranu
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }

    // Klasa wewnętrzna dla panelu gry
    private class SpacePanel extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            draw(g);
        }
    }

    public void draw(Graphics g) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());

        g.setColor(Color.RED);
        g.fillRect(playerX, playerY, 50, 50);

        if (isShooting) {
            g.setColor(Color.YELLOW);
            g.fillRect(bulletX, bulletY, 5, 10);
        }

        for (Enemy enemy : enemies) {
            g.setColor(Color.GREEN);
            g.fillRect(enemy.getX(), enemy.getY(), 30, 30);
        }
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT && playerX > 0) {
            playerX -= playerSpeed;
        } else if (key == KeyEvent.VK_RIGHT && playerX < getWidth() - 50) {
            playerX += playerSpeed;
        } else if (key == KeyEvent.VK_SPACE && !isShooting) {
            isShooting = true;
            bulletX = playerX + 22;
            bulletY = playerY;
        }
    }

    public void keyReleased(KeyEvent e) {
        // Not needed for this simple example
    }

    public void keyTyped(KeyEvent e) {
        // Not needed for this simple example
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                new SpaceShooter().setVisible(true);
            }
        });
    }
}
